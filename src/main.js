import Vue from 'vue';
import App from './App.vue';
import 'materialize-css/sass/materialize.scss';

new Vue({
  el: '#app',
  render: h => h(App)
})
